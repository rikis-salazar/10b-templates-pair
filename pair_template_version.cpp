/**
   Template specialization.
*/



#include <iostream>
#include <string>

using std::cout;
using std::string;
using std::ostream;

/**     C L A S S   I N T E R F A C E     **/
template<typename F,typename S>
class Pair{
   private:
      F first;
      S second;

   public:
      Pair() = delete ;
      Pair(F _f , const S& _s ) : first(_f), second(_s) {}

      // Setter
      void set_values(F _f, const S& _s){
         first = _f;
	 second = _s;
      }

      // Getters
      F get_first() const { return first; }
      S get_second() const { return second; }

      // Member operator (replaced by non-member)
      // Pair<F,S> operator+(const Pair<F,S>&) const;

};



/** ************************* NON-MEMBERS ************************* */

template<typename F, typename S>
void print_values(const Pair<F,S>& _p){    

   F local_first = _p.get_first();
   S local_second = _p.get_second();

   cout << "\tfirst: " << local_first 
        << "\tsecond: " << local_second << "\n";

   return;
}



// operator<<
template <typename F, typename S>
ostream& operator<<(ostream& _out, const Pair<F,S>& _rhs){
   _out << "(" << _rhs.get_first() << "," << _rhs.get_second() << ")";
   return _out;
}

/** ************************* NON MEMBERS ************************* */

template<typename F,typename S>
Pair<F,S> operator+(const Pair<F,S>& _lhs,
                    const Pair<F,S>& _rhs) { 
   return Pair<F,S>( _lhs.get_first() + _rhs.get_first(),
                     _lhs.get_second() + _rhs.get_second() );
}


// _________ TEMPLATE SPECIALIZATION __________
// If one [or both] of the parameters is a string, we use 
// the symbol '-' to separate the entries.
// Notice that we use nonmember addition in this case.

template<typename F>
Pair<F,string> operator+(const Pair<F,string>& _lhs,
                         const Pair<F,string>& _rhs) { 

   return Pair<F,string>( _lhs.get_first() + _rhs.get_first(),
                          _lhs.get_second() + "-" + _rhs.get_second() );
}

template<typename S>
Pair<string,S> operator+(const Pair<string,S>& _lhs,
                         const Pair<string,S>& _rhs) { 

   return Pair<string,S>( _lhs.get_first() + "-" + _rhs.get_first(),
                          _lhs.get_second() + _rhs.get_second() );
}


Pair<string,string> operator+(const Pair<string,string>& _lhs,
                              const Pair<string,string>& _rhs) { 

   return Pair<string,string>( _lhs.get_first() + "-" + _rhs.get_first(),
                               _lhs.get_second() + "-" + _rhs.get_second() );
}



/** ************************* TESTER ************************* */

int main(){
   cout << "Hello, world! (yet one more time)\n";

   Pair<int,string> p0(0,"");
   cout << "p0:"; 
   // We no longer need to help the compiler
   //    print_values<int,string>(p0);  
   // It figures things out by itself. Why?
   print_values(p0);  

   Pair<int,string> p1(1,"Uno");
   cout << "p1:"; 
   // Of course, we can still tell the compiler what it already knows 
   print_values<int,string>(p1); 

   Pair<int,string> p2 = Pair<int,string>(2,"Two"); 
   p2.set_values(2,"Dos");       // Spanish, please.
   cout << "p2:"; 
   print_values(p2); 

   Pair<int,string> p = p1 + p2;
   cout << "\nTesting operator<< and operator+\n";
   cout << "p1+p2:\t" << p  << "\n";
   cout << "p2+p2:\t" << p2+p2  << "\n";
   cout << "Notice that we are missing the '-' separator.\n";
   cout << "To fix this we need template specialization.\n";


   // Both param are now generic!!!
   Pair<string,double> p3("Pi",3.1416);    
   Pair<string,bool> p4("Falso",false);
   p4.set_values("Cierto",true);

   cout << "\n";
   cout << "p3:\t" << p3 << "\n";
   cout << "p4:\t" << p4 << "\n";

   cout << "\n";
   cout << "p3+p3:\t" << p3+p3  << "\n";
   cout << "p4+p4:\t" << p4+p4  << "\n";

   cout << "\n";
   Pair<string,string> p5("Library","Biblioteca");
   Pair<bool,int> p6(true,-1);

   cout << "p5:\t" << p5 << "\n";
   cout << "p6:\t" << p6 << "\n";

   cout << "\n";
   cout << "p5+p5:\t" << p5+p5  << "\n";
   cout << "p6+p6:\t" << p6+p6  << "\n";

   return 0;
}

