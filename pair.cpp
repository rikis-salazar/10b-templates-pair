/**
    The wishlist:

    -- Constructors:
       + No param constructor (if possible)
       + Two param constructor 

    -- Member functions:
       + Getters and Setters 

    -- Non-members:
       + Function that displays contents of pair.
       
    -- Operator:
       + Overload of operator<< (e.g., output to console)
       + Overload and specialization of operator+ 
         * Numeric types should be added,
	 * String types should be concatenated.


   The starting point:

   -- Class interface (no templates yet)
   -- Definition of functions ( stubs only )
   -- Non-functional tester ( driver )
*/



#include <iostream>
#include <string>


/**     C L A S S   I N T E R F A C E     **/
class Pair{
   private:
      int first;
      std::string second;

   public:
      // Constructors
      Pair();
      Pair(int, const std::string&);

      // Setter
      void set_values(int, const std::string&);

      // Getters
      int get_first() const;
      std::string get_second() const;

      // Member operator
      Pair operator+(const Pair&) const;

      // Non-member operator (Friendship is not needed. Why?)
      friend std::ostream& operator<<(std::ostream&, const Pair&);
};

// Non-members 
void print_values(const Pair&);



/**    
    D E F I N I T I O N S    

    Note: function parameters start with the underscore character. 
          ( Pair _p, int _f, std::string _s, etc. )
*/
Pair::Pair(){ 
   first = 0; // <-- 0 is the 'default' int
   second = ""; // <-- the empty string is the 'default' string
}


Pair::Pair(int _f, const std::string& _s)
   : first(_f), second(_s) { }  // <-- Initializer list & empty body


void Pair::set_values(int _f, const std::string& _s){ 
   first = _f;
   second = _s;
   return;
}


int Pair::get_first() const { 
   return first;
}


std::string Pair::get_second() const {
   return second;
}


Pair Pair::operator+(const Pair& _rhs) const { 
   Pair clone(*this);                     // copy of the calling object 
   clone.first += _rhs.first;             // addition
   clone.second += "-" + _rhs.second;     // concatenation
   return clone; 
}


std::ostream& operator<<(std::ostream& _out, const Pair& _rhs){ 
   _out << "(" << _rhs.first << "," << _rhs.second << ")";
   return _out; 
}


void print_values(const Pair& _p){
   std::cout << "\tfirst: " << _p.get_first() 
             << "\tsecond: " << _p.get_second() << "\n";

   return;
}




/**     T E S T E R     **/
int main(){
   std::cout << "Hello, world! (yet one more time)\n";

   Pair p0;                      // <-- Default constr
   Pair p1(1,"Uno");             // <-- Two param constr

   Pair p2 = Pair();             // <-- Default constr
   p2.set_values(2,"Dos");       // <-- Setter


   std::cout << "p0:"; 
   print_values(p0);

   std::cout << "p1:"; 
   print_values(p1);

   std::cout << "p2:"; 
   print_values(p2);

   Pair p = p1+p2;
   std::cout << "p1+p2:"; 
   print_values(p);


   std::cout << "\nUsing operator<<\n";
   std::cout << "p0:\t" << p0 << "\n";
   std::cout << "p1:\t" << p1 << "\n";
   std::cout << "p2:\t" << p2 << "\n";
   std::cout << "p1+p2:\t" << p1+p2 << "\n";
   return 0;
}
