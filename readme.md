## The Pair class

Implementation of a template Pair class.
We briefly discuss

* template _non-member_ functions,
* template _member_ functions, and
* template _specialization_.
